package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsUserRole;

public interface UpmsUserRoleMapper extends SsmMapper<UpmsUserRole,Integer> {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsUserRole record);

    int insertSelective(UpmsUserRole record);

    UpmsUserRole selectByPrimaryKey(Integer userRoleId);

    int updateByPrimaryKeySelective(UpmsUserRole record);

    int updateByPrimaryKey(UpmsUserRole record);
}